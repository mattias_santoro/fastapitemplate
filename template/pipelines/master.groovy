properties([
    pipelineTriggers([
        [
            $class  : 'BitBucketPPRTrigger',
            triggers: [
                [
                    $class      : 'BitBucketPPRRepositoryTriggerFilter',
                    actionFilter: [
                        $class                     : 'BitBucketPPRRepositoryPushActionFilter',
                        triggerAlsoIfNothingChanged: false,
                        triggerAlsoIfTagPush       : false,
                        allowedBranches            : 'master'
                    ]
                ]
            ]
        ]
    ])
])


pipeline {
    agent {
        node {
            label 'ca-node-00'
        }
    }
    options {
        timestamps()
        buildDiscarder(logRotator(numToKeepStr: '5'))
        disableConcurrentBuilds()
    }
    parameters {
        string(name: 'DEPLOY_VERSION', defaultValue: 'latest', description: 'To perform a rollback put the version to rollback (v1.2.0)')
        booleanParam(name: 'ONLY_PRODUCTION', defaultValue: false, description: 'When deploy version is not latest, decide to deploy only production or rollback also the stage environment')
    }

    stages {
        stage('Evaluate version') {
            steps {
                script {
                    version = evaluate_next_version.getVersion([
                        'deploy_version': params.DEPLOY_VERSION
                    ])
                    if (params.DEPLOY_VERSION == 'latest') {
                        buildName String.format("#%s %s", env.BUILD_NUMBER, version)
                        notify_bitbucket(buildState: 'INPROGRESS')
                    } else {
                        buildName String.format("#%s ROLLBACK to %s", env.BUILD_NUMBER, version)
                    }
                }
            }
        }

        stage('Test') {
            when {
                environment name: 'DEPLOY_VERSION', value: 'latest'
            }
            steps {
                sh '/usr/local/bin/docker-compose down'
                sh '/usr/local/bin/docker-compose rm -f -s -v'
                sh '/usr/local/bin/docker-compose up --build --exit-code-from ${{values.microservice_id}}-test ${{values.microservice_id}}-test'
                script {
                    if (!check_test_result('${{values.microservice_id}}-test')) {
                        abort_pipeline("Test aborted")
                    }
                }
            }
        }

        stage('Build') {
            when {
                environment name: 'DEPLOY_VERSION', value: 'latest'
            }
            steps {
                script {
                    ${{values.microservice_id}} = docker_compose_build('${{values.microservice_id}}-webapp', "cloudacademy/${{values.microservice_id}}")
                }
            }
        }

        stage('Cross Env Stack') {
            when {
                environment name: 'DEPLOY_VERSION', value: 'latest'
            }
            stages {
                stage('Template') {
                    steps {
                        on_deploy_folder('production', {
                            script {
                                cf_template_service_update.update([
                                    service                 : '${{values.microservice_id|replace("_", "-")}}',
                                    new_version         : version,
                                    environment         : 'production',
                                    template_file_name  : 'cross-env-stack.template'
                                ])
                                s3_service_upload.upload([
                                    environment       : 'production',
                                    service           : '${{values.microservice_id|replace("_", "-")}}',
                                    new_version       : version,
                                    template_file_name: 'cross-env-stack.template',
                                ])
                            }
                        })
                    }
                }
                stage('Stack') {
                    steps {
                        on_deploy_folder('production', {
                            script {
                                cf_service_update.update([
                                    environment       : 'production',
                                    service           : '${{values.microservice_id|replace("_", "-")}}',
                                    new_version       : version,
                                    stack_name_format : 'environment_as_prefix',
                                    template_file_name: 'cross-env-stack.template',
                                    stack_name_prefix : 'cross-env-stack'
                                ])
                                cf_check_deploy.check([
                                    environment       : 'production',
                                    service           : '${{values.microservice_id|replace("_", "-")}}',
                                    timeout_in_minutes: 10,
                                    stack_name_format : 'environment_as_prefix',
                                    stack_name_prefix : 'cross-env-stack'
                                ])
                            }
                        })
                    }
                }
            }
        }

        stage('Push Image') {
            when {
                environment name: 'DEPLOY_VERSION', value: 'latest'
            }
            steps {
                script {
                    with_aws_docker_registry {
                        ${{values.microservice_id}}.push 'latest'
                        ${{values.microservice_id}}.push version
                    }
                    git_push_tags version
                }
            }
        }

        stage('Build Templates') {
            when {
                environment name: 'DEPLOY_VERSION', value: 'latest'
            }
            stages {
                {%- if values.deploy_bucket == 'true' %}
                stage('Development') {
                    steps {
                        on_deploy_folder('development', {
                            script {
                                cf_template_service_update.update([
                                    service    : '${{values.microservice_id|replace("_", "-")}}',
                                    new_version: version,
                                ])
                                s3_service_upload.upload([
                                    environment: 'development',
                                    service    : '${{values.microservice_id|replace("_", "-")}}',
                                    new_version: version
                                ])
                            }
                        })
                    }
                }
                {%- endif %}
                stage('Stage') {
                    steps {
                        on_deploy_folder('stage', {
                            script {
                                cf_template_service_update.update([
                                    service    : '${{values.microservice_id|replace("_", "-")}}',
                                    new_version: version,
                                ])
                                s3_service_upload.upload([
                                    environment: 'stage',
                                    service    : '${{values.microservice_id|replace("_", "-")}}',
                                    new_version: version
                                ])
                            }
                        })
                    }
                }
                stage('Production') {
                    steps {
                        on_deploy_folder('production', {
                            script {
                                cf_template_service_update.update([
                                    service    : '${{values.microservice_id|replace("_", "-")}}',
                                    new_version: version,
                                ])
                                s3_service_upload.upload([
                                    environment: 'production',
                                    service    : '${{values.microservice_id|replace("_", "-")}}',
                                    new_version: version
                                ])
                            }
                        })
                    }
                }
            }
        }

        stage('Create Artifact') {
            steps {
                script {
                    create_artifact([
                        new_version: version,
                        service: '${{values.microservice_id|replace("_", "-")}}',
                        is_latest: params.DEPLOY_VERSION == 'latest'
                    ])
                }
            }
        }

        stage('Stage') {
            steps {
                script {
                    if (params.DEPLOY_VERSION == 'latest' || !params.ONLY_PRODUCTION) {
                        aws_utils.start_pipeline([
                            pipeline_name: aws_utils.get_stack_output([
                                stack_name: 'production-cross-env-stack-${{values.microservice_id|replace("_", "-")}}',
                                output_name: 'oPreProductionPipelineName'
                            ]),
                            wait: true,
                            timeout_in_minutes: 40
                        ])
                        slackSend channel: '${{values.slack_channel}}', color: '#FF00FF', message: String.format('Hey, ${{values.microservice_title|capitalize}} is waiting your approval!\n\nCheck the build at %s', env.BUILD_URL)
                        input_timeout(2, 'Do you want to deploy on production?')
                    }
                }
            }
        }

        stage('Production') {
            steps {
                script {
                    aws_utils.start_pipeline([
                        pipeline_name: aws_utils.get_stack_output([
                            stack_name: 'production-cross-env-stack-${{values.microservice_id|replace("_", "-")}}',
                            output_name: 'oProductionPipelineName'
                        ]),
                        wait: true,
                        timeout_in_minutes: 40
                    ])
                }
            }
        }

    }
    post {
        success {
            script {
                if (params.DEPLOY_VERSION == 'latest') {
                    slackSend channel: '${{values.slack_channel}}', color: '#00C851', message: String.format('${{values.microservice_title|capitalize}} version %s deploy completed', version)
                    notify_bitbucket(buildState: 'SUCCESSFUL')
                } else {
                    slackSend channel: '${{values.slack_channel}}', color: '#00C851', message: String.format('${{values.microservice_title|capitalize}} rollback to version %s completed', version)
                }
            }
        }
        cleanup {
            sh '/usr/local/bin/docker-compose down'
            deleteDir()
        }
        aborted {
            slackSend channel: '${{values.slack_channel}}', color: '#FFA500', message: String.format('Someone aborted ${{values.microservice_title|capitalize}} version %s!\n\nCheck the build at %s', version, env.BUILD_URL)
        }
        failure {
            script {
                if (params.DEPLOY_VERSION == 'latest') {
                    slackSend channel: '${{values.slack_channel}}', color: '#FF0000', message: String.format('Hey, ${{values.microservice_title|capitalize}} version %s deploy failed!\n\nCheck the build at %s', version, env.BUILD_URL)
                } else {
                    slackSend channel: '${{values.slack_channel}}', color: '#FF0000', message: String.format('Hey, ${{values.microservice_title|capitalize}} rollback to version %s failed!\n\nCheck the build at %s', version, env.BUILD_URL)
                }
            }
        }
        unsuccessful {
            script {
                notify_bitbucket(buildState: 'FAILED')
            }
        }
    }
}
