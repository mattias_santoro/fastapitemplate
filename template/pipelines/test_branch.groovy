pipeline {
    agent {
        node {
            label 'ca-node-00'
        }
    }
    options {
        timestamps()
        buildDiscarder(logRotator(numToKeepStr: '5'))
    }
    parameters {
        string(name: 'BRANCH_NAME', defaultValue: '', description: 'Name of the branch to test')

        booleanParam(name: 'TEST_ENABLE_TIME_REPORT', defaultValue: false, description: 'Enabling this will display the output of the clouda.tests.utils.time_logging_runner.TimeLoggingTestRunner report')

        string(name: 'TEST_NUMBER_OF_PROCCESS', defaultValue: '4', description: 'Number of processes to use during running tests. Basically the --parallel parameter')

        string(name: 'TEST_VERBOSITY', defaultValue: '1', description: 'The --verbosity value passed to the command python manage.py test')
    }

    stages {
        stage('Cleanup') {
            steps {
                deleteDir()
            }
        }
        stage('Checkout') {
            steps {
                checkout_branch('git@bitbucket.org:cloudacademy/${{values.microservice_id}}.git', env.BRANCH_NAME)
                buildDescription env.BRANCH_NAME
            }
        }
        stage('Tests') {
            steps {
                notify_bitbucket(buildState: 'INPROGRESS')
                sh '/usr/local/bin/docker-compose down'
                sh '/usr/local/bin/docker-compose rm -f -s -v'
                sh '/usr/local/bin/docker-compose up --build --exit-code-from ${{values.microservice_id}}-test ${{values.microservice_id}}-test'
                script {
                    if (!check_test_result('${{values.microservice_id}}-test')) {
                        abort_pipeline("Test aborted")
                    }
                }
            }
        }
    }
    post {
        cleanup {
            sh '/usr/local/bin/docker-compose down'
            sh '/usr/local/bin/docker-compose rm -f -s -v'
            deleteDir()
        }
        success {
            notify_bitbucket(buildState: 'SUCCESSFUL')
        }
        unsuccessful {
            notify_bitbucket(buildState: 'FAILED')
        }
    }
}
