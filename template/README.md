# ${{values.microservice_title}}

${{values.microservice_description}}

{%- if values.use_authentication == 'true' -%}

## Authentication

To let authentication works on development and integration environments you have to ensure to exports the following env vars
that allows you to configure the Auth0 and Bakery JWT issuers.

```bash
JWT_AUTH_AUTH0_ISSUER={"issuer": "https://cloudacademy-dev.auth0.com/", "audience":"https://services.cloudacademy.com/api/"}
JWT_AUTH_CLOUDACADEMY_ISSUER={"public_key": "-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAv7BRetjM60OdqkAxnEIx\nSSW2A6zJsVDA6FgvWam2celHjkEu7DQz58qjExMAZyxjXgbM9jZE96GSbftbn9g0\nTuSDpV0jWuXZr+N7zAN/NJRxIRZwGlZIsjBJjVMyKs0QHX6JNnn5FhhBriXZOQX7\nWOoZI+m/fCaBTAfpsDUWFzydnDtda3D3RmSY0SJTpY9vjWUPThDeM2eawFWuU+3G\nKzjMOYgbRF/cM89eKW7/TOfvYiiqs1/p3LCiakfJ4R1J6+pJ7vs6VfTZUTCE2E92\nQsHxHQqEPIRpf0dzdr2NNaz+6EjGVfzk7xIymhWdNnExUioZ9anRySE5VkrCf1X/\nw1On29tXDJR8hn2qhBfhAHi7bEo7rEHGaJgmaWgNCoxGmx2lFaVpVkMQKnEHd5MM\neUVrsvXXzedayy5jadoYwxyAq0YsK7oDoFaPNEvTmsU5cuoGgdevqJCvIjEIIzQg\n5WA3xKYfaqccEhWsGU04FcYa6DOQPkjxNDGK+pJK2uNmRiUUpjYfUyoMn+210hO5\nt9hH0hrILMWyznm59/G4L7MjQGyzvUxyHjt8Y93uPIHJRYTiKq7Y3eJ7zUHXaTYO\njVTufdV7Zb7MunaQkl0ejk6crX15fqdrcxUqIg6LO1ywpwtpq7YbofcEbNbVO80U\negvvekyWb5tzZg5HpY6epYsCAwEAAQ\=\=\n-----END PUBLIC KEY-----\n"}
```

You can evaluate to use the pydantic envfile feature to handle them.

### OAuth2 scopes

Using ca.fastapi-jwt, you can easily validate OAuth2 scopes related to your service declaring them endpoint per endpoint.

For example, let suppose that you expect to have the scope 'preferences:update', this would be the code to require
an authenticated JWT with that scope.


```python
from fastapi import APIRouter, Security
from app.dependencies.context import get_authenticated_context
from fastapi_jwt.dependencies import get_current_jwt_requester
from fastapi_jwt.requester import JWTRequesterData

preferences_router = APIRouter()


@preferences_router.patch(path="/preferences/")
async def integration_authenticated_api(context: AuthenticatedContext = Security(get_authenticated_context, scopes=['preferences:update'])):
    return {
        'sub': requester.subject,
        'issuer': requester.issuer,
        'is_service_user': requester.is_service_user
    }
```

It the required scopes are not present in the JWT (this is true only for Auth0 authentication) the service will respond with 403.

{%- endif -%}

{%- if values.use_database == 'true' %}
### In case of missing pg_config:

#### Install postgresql

```bash
brew install postgresql
```

Then link it 

```
brew link postgresql
```

### In case of missing SSL library:

#### Verify the actual openssl version with

```
brew info python
```

This should indicate you something like 'openssl@X.Y'. 
Ask brew for more info:

```
brew info openssl@X.Y
```

Follow the instructions to make the library available for compilers in the current shell, an example below:

#### Example of export

```
export LDFLAGS="-L/usr/local/opt/openssl@1.1/lib"
export CPPFLAGS="-I/usr/local/opt/openssl@1.1/include"
```

The relaunch the installation with the following command:

```
pip install --extra-index-url https://pypi.production.svc.cloudacademy.xyz/ -r requirements.txt
```
{%- endif %}
