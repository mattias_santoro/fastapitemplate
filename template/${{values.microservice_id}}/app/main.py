import argparse

import ca.logs
import rollbar
import uvicorn
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi import FastAPI, APIRouter
from starlette.responses import JSONResponse
from app.routers.integration import integration_router

from app.middlewares import RollbarMiddleware, TenantMiddleware
from app.settings import (
    ROLLBAR,
    ENVIRONMENT,
    APM_TOKEN,
    APM_ENDPOINT,
    ENABLE_ELASTIC_APM,
    APP_VERSION,
)

apm = make_apm_client(({
    'SERVICE_NAME': '${{values.microservice_title|capitalize|replace(' ', '')}}',
    'SECRET_TOKEN': APM_TOKEN,
    'SERVER_URL': APM_ENDPOINT,
    'COLLECT_LOCAL_VARIABLES': 'errors',
    'CAPTURE_BODY': 'errors',
    'ENABLED': ENABLE_ELASTIC_APM,
    'DEBUG': False,
    'DISABLE_METRICS': "*",
    'BREAKDOWN_METRICS': False,
    'SERVICE_VERSION': APP_VERSION,
    'ENVIRONMENT': ENVIRONMENT,
    'TRANSACTIONS_IGNORE_PATTERNS': [
        'GET /health-check/',
        'GET /favicon.ico'
    ],
}))

app = FastAPI(title='${{values.microservice_title}}', description='${{values.microservice_description}}')

# Middlewares installation
app.add_middleware(RollbarMiddleware)
if ENABLE_ELASTIC_APM:
    app.add_middleware(ElasticAPM, client=apm)
app.add_middleware(TenantMiddleware)

# Logging
ca.logs.setup('${{values.microservice_title|lower}}')

# Rollbar initialization
rollbar.init(**ROLLBAR)

# Routing configs
app.add_route(
    "/health-check/", lambda req: JSONResponse(
        {
            "environment": ENVIRONMENT,
            "healthy": True,
            "version": APP_VERSION,
        }
    )
)
# Main routers for API versioning
router_api = APIRouter()
router_api_v1 = APIRouter()
# Registration of routes
router_api_v1.include_router(integration_router, prefix="/integration-tests")
# Finalizing setup of router with FastAPI app
router_api.include_router(router_api_v1, prefix="/v1")
app.include_router(router_api, prefix="/api")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int)
    parser.add_argument('--host', type=str)
    parser.add_argument('--reload', action='store_true')

    args = parser.parse_args()
    host = args.host
    port = args.port
    reload = args.reload

    uvicorn.run('main:app', host=host or "0.0.0.0", port=port or ${{values.microservice_port}}, reload=reload)
