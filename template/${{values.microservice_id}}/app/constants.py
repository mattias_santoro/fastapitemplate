HTTP_HEADER_TENANT = 'X-CloudAcademy-Tenant-Tag'
HTTP_HEADER_AUTHORIZATION = 'Authorization'
HTTP_HEADER_REFERER = 'X-CloudAcademy-Referer'
HTTP_HEADER_CONTENT_TYPE = 'Content-Type'

TENANT_HEADER_EXCLUDED_ROUTES = [
    '/health-check',
    '/docs',
    '/favicon',
    '/openapi'
]
TENANT_REQUEST_SCOPE_KEY = 'tenant'
