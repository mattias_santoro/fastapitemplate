import enum


class Tenant(enum.Enum):
    CLOUDACADEMY = 'cloudacademy'
    QA = 'qa'
