import os

ENVIRONMENT = os.environ.get('ENVIRONMENT', 'local')
ROLLBAR_ACCESS_TOKEN = os.environ.get('ROLLBAR_ACCESS_TOKEN')

ROLLBAR = {
    'access_token': ROLLBAR_ACCESS_TOKEN,
    'environment': ENVIRONMENT,
    'enabled': ENVIRONMENT != 'local',
}

TENANTS = {
    'qa': {
        'conf': {
        },
    },
    'cloudacademy': {
        'conf': {
        },
    },
    'hpe': {
        'conf': {
        },
    }
}

# APM Settings
APM_TOKEN = os.environ.get('APM_TOKEN', '')
APM_ENDPOINT = os.environ.get('APM_ENDPOINT', 'http://beat:8200')
ENABLE_ELASTIC_APM = True if os.environ.get('ENABLE_ELASTIC_APM', False) == 'true' else False
APP_VERSION = os.environ.get('APP_VERSION', 'dev')

{%- if values.deploy_bucket == 'true' %}
# S3 Bucket name
S3_BUCKET = os.environ.get('S3_BUCKET_NAME', "clouda-%s-${{values.microservice_id|replace('_', '-')}}" % os.environ.get('ENVIRONMENT', 'development'))
{%- endif %}