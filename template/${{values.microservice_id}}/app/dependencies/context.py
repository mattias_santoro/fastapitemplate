from fastapi import Depends, Security
from fastapi_jwt.dependencies import get_current_jwt_requester, get_current_service_jwt_requester
from fastapi_jwt.requester import JWTRequesterData
from starlette.requests import Request

from app.context import ContextFactory
from app.services.base.dtos import Context, AuthenticatedContext, AuthenticationPassport


async def get_context(request: Request) -> Context:
    return ContextFactory.from_http_request(request)


async def get_authenticated_context(
    context: Context = Depends(get_context),
    requester: JWTRequesterData = Security(get_current_jwt_requester),
) -> AuthenticatedContext:
    """
    Provides an authenticated context looking for a user/service authenticated with JWT.

    Note that at this level of the dependency injection no OAuth2 scopes are required, so you have to
    ensure to declare them on the API path.
    """
    return AuthenticatedContext(
        tenant=context.tenant, auth_passport=AuthenticationPassport(token=requester.raw_jwt), requester=requester
    )


async def get_s2s_authenticated_context(
    context: Context = Depends(get_context),
    requester: JWTRequesterData = Security(get_current_service_jwt_requester),
) -> AuthenticatedContext:
    """ Provides an authenticated context looking for a service user authenticated with JWT"""
    return AuthenticatedContext(
        tenant=context.tenant, auth_passport=AuthenticationPassport(token=requester.raw_jwt), requester=requester
    )

