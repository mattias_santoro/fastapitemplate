"""
Example of FastAPI router with CA utils.
"""
from fastapi import APIRouter, Security

from fastapi_jwt.dependencies import get_current_jwt_requester
from fastapi_jwt.requester import JWTRequesterData

integration_router = APIRouter()


@integration_router.get(path="/authenticated/")
async def integration_authenticated_api(requester: JWTRequesterData = Security(get_current_jwt_requester)):
    return {
        'sub': requester.subject,
        'issuer': requester.issuer,
        'is_service_user': requester.is_service_user
    }
