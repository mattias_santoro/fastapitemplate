import logging

import rollbar
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from starlette.requests import Request
from starlette.responses import Response, JSONResponse

from app.constants import TENANT_HEADER_EXCLUDED_ROUTES, HTTP_HEADER_TENANT, TENANT_REQUEST_SCOPE_KEY
from app.tenants import Tenant

logger = logging.getLogger(__name__)


class RollbarMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next: RequestResponseEndpoint) -> Response:
        try:
            response = await call_next(request)
        except Exception:
            rollbar.report_exc_info()
            raise
        return response


class TenantMiddleware(BaseHTTPMiddleware):
    """ Middleware that ensure that a valid tenant header is provided and set in the current request """
    async def _need_tenant(self, request: Request) -> bool:
        for excluded_route in TENANT_HEADER_EXCLUDED_ROUTES:
            if request.url.path.startswith(excluded_route):
                return False
        logger.debug("Path %s need tenant", request.url.path)
        return True

    async def dispatch(self, request: Request, call_next: RequestResponseEndpoint) -> Response:
        if await self._need_tenant(request):
            x_cloudacademy_tenant_tag = request.headers.get(HTTP_HEADER_TENANT)
            if x_cloudacademy_tenant_tag is None:
                logger.info("Returning 400 to the client. Path %s miss tenant header", request.url.path)
                response = JSONResponse(
                    status_code=400,
                    content={
                        "message": "Missing tenant header in the request",
                    },
                )
                return response

            try:
                tenant = Tenant[x_cloudacademy_tenant_tag.upper()]
            except KeyError:
                logger.error(
                    "Request for path %s contains a wrong tenant %s",
                    request.url.path,
                    x_cloudacademy_tenant_tag,
                )
                return JSONResponse(status_code=400, content={"message": f"Wrong tenant {x_cloudacademy_tenant_tag}"})

            request.scope[TENANT_REQUEST_SCOPE_KEY] = tenant

        response = await call_next(request)
        return response
