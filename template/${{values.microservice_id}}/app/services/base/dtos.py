from fastapi_jwt.requester import JWTRequesterData
from pydantic import BaseModel

from app.tenants import Tenant


class AuthenticationPassport(BaseModel):
    token: str


class Context(BaseModel):
    tenant: Tenant


class AuthenticatedContext(Context):
    auth_passport: AuthenticationPassport = None
    requester: JWTRequesterData = None
