AWSTemplateFormatVersion: 2010-09-09
Description: 'CloudAcademy repositories: ${{values.microservice_title}} Cross Env Stack'
Transform:
  - CloudAcademy-MicroService

Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
      - Label:
          default: General
        Parameters:
          - pApplicationVersion
          - pApplicationName
          - pSlackChannelId
          - pTenant
          - pCostAllocation
          - pPrettyApplicationName
      - Label:
          default: Pipeline
        Parameters:
          - pArtifactStoreBucket
      - Label:
          default: ECR
        Parameters:
          - pImageMaxCountNumber
    ParameterLabels:
      pArtifactStoreBucket:
        default: Bucket name for artifact storage
      pApplicationName:
        default: Application Name
      pApplicationVersion:
        default: Application version
      pSlackChannelId:
        default: Slack channel id of the service
      pPrettyApplicationName:
        default: Pretty Application Name
      pCostAllocation:
        default: Cost Allocation Tag
      pTenant:
        default: Tenant
      pImageMaxCountNumber:
        default: Number of ECR images to keep

Parameters:
  pApplicationVersion:
    Type: String
  pSlackChannelId:
    Type: String
  pArtifactStoreBucket:
    Type: String
  pApplicationName:
    Type: String
  pImageMaxCountNumber:
    Type: Number
  pPrettyApplicationName:
    Type: String
  pCostAllocation:
    Type: String
    AllowedValues:
      - sre
      - marketing
      - content
      - platform
      - data
      - labs
      - inspire
  pTenant:
    Type: String
    AllowedValues:
      - cloudacademy
      - qa

Resources:
  rCrossEnv:
    Type: CloudAcademy::MicroService::CrossEnv
    Properties:
      ResourceVersion: v1.0.0
      {%- if values.use_database == 'true' %}
      Database:
        DevPSQLData: true
      {%- endif %}
      {%- if values.deploy_rollbar == 'true' or values.slack_channel_id %}
      LoggingAndMonitoring:
        {%- if values.deploy_rollbar == 'true' %}
        DeployRollbar: true
        {%- endif %}
        {%- if values.slack_channel_id %}
        DeploySlackIntegration: true
        {%- endif %}
      {%- endif %}
      Pipeline:
        - Name: pre-production
          Actions:
            {%- if values.deploy_bucket == 'true' %}
            - Name: development
            {%- endif %}
            - Name: stage
        - Name: production
          Actions:
            - Name: production

Outputs:
  oRepositoryName:
    Value: !GetAtt rCrossEnv.rCrossEnvECRRepository
  {%- if values.use_database == 'true' %}
  oDevPSQLDataRepositoryName:
    Value: !GetAtt rCrossEnv.rCrossEnvDevPSQLDataRepository
  {%- endif %}
  oPreProductionPipelineName:
    Value: !GetAtt rCrossEnv.rCrossEnvPreProductionPipeline
  oProductionPipelineName:
    Value: !GetAtt rCrossEnv.rCrossEnvProductionPipeline
  oServiceSNSTopicArnParameterName:
    Value: !GetAtt rCrossEnv.rCrossEnvSNSTopicArnParameter
  {%- if values.deploy_rollbar == 'true' %}
  oRollbarProjectIdParameterName:
    Value: !GetAtt rCrossEnv.rCrossEnvRollbarProjectIdParameter
  {%- endif %}