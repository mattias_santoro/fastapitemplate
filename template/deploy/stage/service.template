AWSTemplateFormatVersion: 2010-09-09
Description: 'Cloudacademy service: ${{values.microservice_title}}'
Transform:
  - CloudAcademy-MicroService

Metadata:
  'AWS::CloudFormation::Interface':
    ParameterGroups:
      - Label:
          default: General
        Parameters:
          - pEnvironment
          - pApplicationVersion
          - pTenant
          - pCostAllocation
          - pPrettyApplicationName
          - pApplicationName
      - Label:
          default: Networking
        Parameters:
          - pVpcId
          - pEcsCluster
          - pEcsInternalALBHTTPSListenerArn
      - Label:
          default: Alerting
        Parameters:
          - pServiceSNSTopicArn
    ParameterLabels:
      pEnvironment:
        default: Environment
      pApplicationVersion:
        Default: Application version
      pVpcId:
        default: VPC Id
      pEcsCluster:
        default: ECS cluster
      pEcsInternalALBHTTPSListenerArn:
        default: ECS internal ALB HTTPS listener
      pCostAllocation:
        default: Cost Allocation Tag
      pTenant:
        default: Tenant
      pPrettyApplicationName:
        default: Pretty Application Name
      pApplicationName:
        default: Application Name
      pServiceSNSTopicArn:
        default: Service SNS Topic Arn

Parameters:
  pEnvironment:
    Type: String
    AllowedValues:
      - stage
  pApplicationVersion:
    Type: String
  pVpcId:
    Type: AWS::SSM::Parameter::Value<AWS::EC2::VPC::Id>
  pEcsCluster:
    Type: AWS::SSM::Parameter::Value<String>
  pEcsInternalALBHTTPSListenerArn:
    Type: AWS::SSM::Parameter::Value<String>
  pCostAllocation:
    Type: String
    AllowedValues:
      - sre
      - marketing
      - content
      - platform
      - data
      - labs
      - inspire
  pTenant:
    Type: String
    AllowedValues:
      - cloudacademy
      - qa
  pPrettyApplicationName:
    Type: String
  pApplicationName:
    Type: String
  pServiceSNSTopicArn:
    Type: AWS::SSM::Parameter::Value<String>
  {%- if values.deploy_rollbar == 'true' %}
  pRollbarProjectId:
    Type: AWS::SSM::Parameter::Value<String>
  {%- endif %}

Resources:
  rMicroService:
    Type: CloudAcademy::MicroService::Standard
    Properties:
      ResourceVersion: v1.0.0
      {%- if values.deploy_bucket == 'true' %}
      S3:
        DeployBucket: true
      {%- endif %}
      {%- if values.use_database == 'true' %}
      Database:
        DeployDatabase: true
        CreateUser: true
        EnableBackup: false
      {%- endif %}
      LoggingAndMonitoring:
        {%- if values.deploy_rollbar == 'true' %}
        DeployRollbar: true
        {%- endif %}
        DeployElastic: true
      Services:
        - Name: Webapp
          Cluster: !Ref pEcsCluster
          VpcId: !Ref pVpcId
          Containers:
            - Name: webapp
              {%- if values.use_authentication == 'true' %}
              ExtraEnvironment:
                - Name: JWT_AUTH_CLOUDACADEMY_ISSUER
                  Value: '{"public_key": "-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEArVOI74uWRR0MQlB1CKPF\n4tzzQnOMvE9qNZEgfERgSzZehO50T73wNKfJ+dzuPRaWI0sWPLe0KUOLRlObu3Ne\nQ80tUW7z+ZPehm2fVFjy+mSnOSvXP5PD+BX0SS604jfs2hwBioluiWrj9USnS9xm\n8uJfoTr9TBwr4Q5oLLGj1Lk/aVK+GPTUMPGSp0K5a9hbFhgbMCzLQKr+MPFPe123\n6uttyP/9cJ3qfCh1ydesxb2eg80OYNHQy3kfNAj4eLKkwv/3iwf7bf0uOoVMyBQv\n+3uQxqEnZMvngFI5cHT//CaAAGa0otifyG7k849oYqGRukt0AUXSLZiG9Tv/4TA4\njZ7Hoq48NFcDmZGGizlPz7ELUvLzAkRC1x+bGWVLKImhs3R1Mol1SoRLE0AAPdHo\nNV7rpBS+oXv7XB0688Xq6YDd4wGOuPZ5KFsxt0nFXmhHX4BD62pxpg88q22h4Fca\nPLmrJUSFX3AQku1vr9U+srtZroat4c+hCZtO4ONGmBNi2A0w/QCZm+buGfUqVbtg\nSsnHSOKGechIMW8CroptVg01/7itBS/JxBRqFQyCVq950qQJssjEp35HL/mAi168\nDQJe2x5/RkPoLiCV3vo+EkZq7npj7NRzWsTEIyiWtyQ3H4ep0dodvGNxiMTziSV0\nh8WHzp16R2Yunpw7adNFFW8CAwEAAQ==\n-----END PUBLIC KEY-----\n" }'
                - Name: JWT_AUTH_AUTH0_ISSUER
                  Value: '{"issuer": "https://cloudacademy-stage.auth0.com/"}'
              {%- endif %}
          LoadBalancers:
            - ContainerPort: ${{values.microservice_port}}
              ContainerName: webapp
              ListenerArn: !Ref pEcsInternalALBHTTPSListenerArn
              Priority: ${{values.microservice_port}}
              HostHeader: !Sub ${{values.microservice_id|replace("_", "-")}}.${pEnvironment}.svc.cloudacademy.xyz
              Alarms:
                - Name: few-healthy-hosts
                - Name: too-many-5xx-requests
                - Name: too-many-requests
              Scaling:
                MinCapacity: 1
                MaxCapacity: 3
